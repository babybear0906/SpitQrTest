package SimpleTest;

import java.util.HashMap;

public class CutTest {
    public static void main(String[] args) {
        String qrInfo =
                "0002010102122629000697043701150007006800000015204999953037045405150005802VN5916PLX_TNB-CT_CH 06600201610610000062490105HDB010324000700680000001-007000230708007000236304DAFB";
        HashMap<String, String> splitValue = splitQR(qrInfo);
        System.out.println("Split value: " + splitValue.get("Point of initiation method"));

    }

    private static HashMap<String, String> splitQR(String value) {
        HashMap<String,String> refQRInfo = new HashMap<>();
        int count = 0;
        int hasMapCount = 0;
        while (count != value.length()) {
            for (int i = count; i < value.length(); i++) {
                String checkAmount = "" + value.charAt(i) + value.charAt(i + 1);
                String connect = "" + value.charAt(i + 2) + value.charAt(i + 3);
                String oneString = "";

                if(Integer.parseInt(checkAmount)!=54&&hasMapCount==5){
                    hasMapCount=6;
                    refQRInfo.put("5","null");
                }
                else{
                for (int j = i; j < count + Integer.parseInt(connect) + 4; j++) {
                    oneString += value.charAt(j);
                }
                    refQRInfo.put(Integer.toString(hasMapCount), oneString);
                count += Integer.parseInt(connect) + 4;
                hasMapCount += 1;
                }
                break;
            }
        }
        HashMap<String,String> qrInfo= getCorrectKey(refQRInfo,hasMapCount-1);
        return qrInfo;
    }

    private static HashMap<String, String> getCorrectKey(HashMap refQR,int count){
        String [] key = {"Payload Format Indicator","Point of initiation method","Merchant Account Information","Merchant Category Code","Transaction Currency Code",
                "Transaction Amount","Country Code","Merchant Name","Merchant City","Postal Code","Additional Data Field","CRC"};
        HashMap<String,String> qrInfo= new HashMap<>();
        for (int i=0; i<count;i++){
            qrInfo.put(key[i],refQR.get(Integer.toString(i)).toString());
        }
        return qrInfo;
    }
}
