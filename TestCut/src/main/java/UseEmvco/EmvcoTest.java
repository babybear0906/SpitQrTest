package UseEmvco;
/**
 * import lib from
 */

import br.com.fluentvalidator.context.ValidationResult;
import com.emv.qrcode.decoder.mpm.AdditionalDataFieldDecoder;
import com.emv.qrcode.decoder.mpm.DecoderMpm;
import com.emv.qrcode.model.mpm.MerchantPresentedMode;
import com.emv.qrcode.validators.MerchantPresentedModeValidate;

import java.util.HashMap;

public class EmvcoTest {
    public static void main(String[] args) {

        String qrInfo =
                "00020101021126300010A00000077501120000000003115204123453037045802VN5909minhan3116003HCM610670000062200307DT3110107053110163047B1D";
        emvcoTest(qrInfo);
    }

    public static void emvcoTest(String encoded) {
        MerchantPresentedMode merchantInfo = DecoderMpm.decode(encoded, MerchantPresentedMode.class);
        ValidationResult validationResult = MerchantPresentedModeValidate.validate(merchantInfo);

        //validate the qrCode
        if (validationResult.isValid()) {
            /**get the subtag of Additional data field**/
            System.out.println("Terminal label:" + merchantInfo.getAdditionalDataField().getValue().getTerminalLabel());
            System.out.println("Globally Unique Identifier: "+getGUID(merchantInfo.getMerchantAccountInformation().get("26").toString()));
            System.out.println("Merchant ID: "+getMechantID(merchantInfo.getMerchantAccountInformation().get("26").toString()));

        } else {
            System.out.println("The QR Code is not valid");
        }
    }

    public static String getMechantID(String data) {
        return getSubtag(data).get("MechantID");
    }

    public static String getGUID(String data) {
        return getSubtag(data).get("GloballyUniqueIdentifier");
    }

    private static HashMap<String, String> getSubtag(String data) {
        int count = 4;
        String nameOfTag[] = {"GloballyUniqueIdentifier", "MechantID"};
        int hasMapCount = 0;
        HashMap<String, String> refQRInfo = new HashMap<>();
        while (count != data.length()) {
            for (int i = count; i < data.length(); i++) {
                String amountOfSubtag = "" + data.charAt(i + 2) + data.charAt(i + 3);
                String oneString = "";
                //add chars of subtag into a string
                for (int j = i + 4; j < count + Integer.parseInt(amountOfSubtag) + 4; j++) {
                    oneString += data.charAt(j);
                }
                refQRInfo.put(nameOfTag[hasMapCount], oneString);
                count += Integer.parseInt(amountOfSubtag) + 4;
                hasMapCount += 1;
                break;
            }
        }
        return refQRInfo;
    }
}
